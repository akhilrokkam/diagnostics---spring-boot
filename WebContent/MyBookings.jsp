<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
<!-- Mobile Specific Meta -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Favicon-->
<link rel="shortcut icon" href="img/fav.png">
<!-- Author Meta -->
<meta name="author" content="colorlib">
<!-- Meta Description -->
<meta name="description" content="">
<!-- Meta Keyword -->
<meta name="keywords" content="">
<!-- meta character set -->
<meta charset="UTF-8">
<!-- Site Title -->
<title>Medical</title>

<link
	href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700"
	rel="stylesheet">
<!-- CSS============================================= -->
<link rel="stylesheet" href="${contextPath}/akhil/css/linearicons.css">
<link rel="stylesheet"
	href="${contextPath}/akhil/css/font-awesome.min.css">
<link rel="stylesheet" href="${contextPath}/akhil/css/bootstrap.css">
<link rel="stylesheet"
	href="${contextPath}/akhil/akhil/css/magnific-popup.css">
<link rel="stylesheet" href="${contextPath}/akhil/css/jquery-ui.css">
<link rel="stylesheet" href="${contextPath}/akhil/css/nice-select.css">
<link rel="stylesheet" href="${contextPath}/akhil/css/animate.min.css">
<link rel="stylesheet" href="${contextPath}/akhil/css/owl.carousel.css">
<link rel="stylesheet" href="${contextPath}/akhil/css/jquery-ui.css">
<link rel="stylesheet" href="${contextPath}/akhil/css/main.css">
</head>
<body>
	<header id="header">
		<div class="container main-menu">
			<div class="row align-items-center justify-content-between d-flex">
				<div id="logo">
					<a href="${contextPath}/uhome"><img src="${contextPath}/akhil/img/logo.png" alt="" title="" /></a>
				</div>
				<nav id="nav-menu-container">
					<ul class="nav-menu">
						<li><a href="${contextPath}/uhome">Home</a></li>
						<li><c:if
								test="${pageContext.request.userPrincipal.name != null}">
								<form id="logoutForm" method="POST"
									action="${contextPath}/logout">
									<input type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}" />
								</form>
								<li><a><font color="white">${pageContext.request.userPrincipal.name}</font></a></li>
								<li><a onclick="document.forms['logoutForm'].submit()"><font
										color="white">Logout</font></a></li>
							</c:if></li>
					</ul>
				</nav>
			</div>
		</div>
	</header>
	<!-- #header -->

	<!-- start banner Area -->
	<section class="banner-area relative" id="home">
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div
				class="row fullscreen d-flex align-items-center justify-content-center">
				<div class="banner-content col-lg-8 col-md-12">
					<h1>We Care for Your Health Every Moment</h1>
					<p class="pt-10 pb-10 text-white">It is the duty of a doctor to prolong life and it is not his duty to prolong the act of dying.</p>
				</div>
			</div>
		</div>
	</section>
	<br> <br>
	<div>
	<h1 align="center">My Bookings</h1>
	<br>
		<font color="black"><table class="table">
			<tr>
				<th>Booking_Id</th>
				<!-- <th>User Name</th> -->
				<th>Lab Name</th>
				<th>Test Name</th>
				<th>Date</th>
				<th>Slot</th>
				<th>Personal Details</th>
				<th>status</th>
				<th>Prescription</th>

			</tr>
			<c:forEach var="bookin" items="${bookinglist}">
				<tr>
					<td>${bookin.bid}</td>
					<%-- <td>${bookin.username}</td> --%>
					<td>${bookin.labname}</td>
					<td>${bookin.testname}</td>
					<td>${bookin.date}</td>
					<td>${bookin.slot}</td>
					<td>${bookin.personaldetails}</td>
					<td>${bookin.status}</td>
					<td><a href="${contextPath}/${bookin.path}/view">View Prescription</a></td> <%-- <embed src="${contextPath}/images/${bookin.path}" />--%>
				</tr>
			</c:forEach>
		</table></font>
	</div>


	<script src="${contextPath}/akhil/js/vendor/jquery-2.2.4.min.js"></script>
	<script src="${contextPath}/akhil/js/popper.min.js"></script>
	<script src="${contextPath}/akhil/js/vendor/bootstrap.min.js"></script>
	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
	<script src="${contextPath}/akhil/js/jquery-ui.js"></script>
	<script src="${contextPath}/akhil/js/easing.min.js"></script>
	<script src="${contextPath}/akhil/js/hoverIntent.js"></script>
	<script src="${contextPath}/akhil/js/superfish.min.js"></script>
	<script src="${contextPath}/akhil/js/jquery.ajaxchimp.min.js"></script>
	<script src="${contextPath}/akhil/js/jquery.magnific-popup.min.js"></script>
	<script src="${contextPath}/akhil/js/jquery.tabs.min.js"></script>
	<script src="${contextPath}/akhil/js/jquery.nice-select.min.js"></script>
	<script src="${contextPath}/akhil/js/owl.carousel.min.js"></script>
	<script src="${contextPath}/akhil/js/mail-script.js"></script>
	<script src="${contextPath}/akhil/js/main.js"></script>
</body>
</html>